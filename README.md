# ads
**Classified Ads App**

To run this app, follow these steps:
 
1. Clone the repository:   
```git clone https://github.com/MargaritaPopova/ads.git```

3. Make sure your Docker is running

2. Switch to project root:   
```cd djangoProject```

5. Run the following command from the project root:   
```docker-compose up --build```
